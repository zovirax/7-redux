import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles.module.css'

class ChatHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {chatName, membersCount, messagesCount, lastMessageTime} = this.props;

        return (
            <header className={styles.chat_header}>
                <div className={styles.right_side}>
                    <pre title={'Chat name'}>{chatName}</pre>
                    <div className={styles.item_icon} title={'Total members'}>
                        <i className="large material-icons">people</i>
                        <span>{membersCount}</span>
                    </div>
                    <div className={styles.item_icon} title={'Total messages'}>
                        <i className="large material-icons">message</i>
                        <span>{messagesCount}</span>
                    </div>
                </div>
                <span>Last message at {lastMessageTime}</span>
            </header>
        );
    }
}

ChatHeader.propTypes = {
    chatName: PropTypes.string.isRequired,
    membersCount: PropTypes.number.isRequired,
    messagesCount: PropTypes.number.isRequired,
    lastMessageTime: PropTypes.string.isRequired
};

export default ChatHeader;
