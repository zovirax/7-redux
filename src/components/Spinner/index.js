import React from 'react';

import styles from './styles.module.css';

class Spinner extends React.Component{
    render() {
        return (
            <div className={styles.spinner_container}>
                <div className={styles.circle}></div>
                <div className={styles.small_circle}></div>
            </div>
        );
    }
}

export default Spinner;
