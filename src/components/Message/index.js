import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles.module.css';

class Message extends React.Component {
    render() {
        const {id, text, createdAt, user, editedAt, avatar, isOwn, isNew, deleteMessage, editMessage} = this.props;

        const containerClass = `${styles.message_container} ${isOwn ? styles.own : ''}`;
        return (
            <div className={containerClass}>
                {isOwn ? null : <div className={styles.sender_info}>
                    <a href={avatar}>
                        <img src={avatar} alt="Avatar" className={styles.avatar}/>
                    </a>
                </div>}
                <div className={styles.message_content + ` ${isNew ? styles.new_message : ''}`}>
                    {isOwn ? null : <a href="#" className={styles.sender_username}>{user}</a>}
                    <p className={styles.message_body}>{text}</p>
                    <div className={styles.message_footer}>
                        <pre title={`edited at ${editedAt}`}>{editedAt ? 'edited\t' : null}</pre>
                        <span className={styles.message_date}>{createdAt}</span>
                        {isOwn ? <div className={styles.icons_container_footer}>
                            <i className="material-icons" onClick={() => deleteMessage(id)}>delete</i>
                            <i className="material-icons" onClick={() => editMessage(id)}>create</i>
                        </div> : null}
                    </div>
                </div>
            </div>
        );
    }
}

Message.propTypes = {
    id: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    editedAt: PropTypes.string.isRequired,
    avatar: PropTypes.string,
    isOwn: PropTypes.bool.isRequired,
    isNew: PropTypes.bool,
    deleteMessage: PropTypes.func.isRequired,
    editMessage: PropTypes.func.isRequired,
};

Message.defaultProps = {
    avatar: 'https://ramcotubular.com/wp-content/uploads/default-avatar.jpg'
};

export default Message;
