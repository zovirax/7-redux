import {SHOW_MODAL, HIDE_MODAL, SET_CURRENT_COMMENT_ID, DROP_CURRENT_COMMENT_ID} from './actionTypes';

export const showModal = () => ({
    type: SHOW_MODAL,
})

export const hideModal = () => ({
    type: HIDE_MODAL,
})

export const setCurrentCommentId = id => ({
    type: SET_CURRENT_COMMENT_ID,
    payload: {
        id
    }
})

export const dropCurrentCommentId = () => ({
    type: DROP_CURRENT_COMMENT_ID,
})
