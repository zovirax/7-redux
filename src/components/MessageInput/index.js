import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles.module.css'

class MessageInput extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            message: '',
        }
    }

    handleChange = event => {
        const message = event.target.value;
        this.setState({message});
    }

    handleSubmit = () => {
        const message = this.state.message;
        if (message.trim()) {
            this.props.sendMessage(message);
            this.setState({message: ''});
        }
    }

    render() {
        return (
            <div className={styles.input_container}>
                <div className={styles.input}>
                    <input value={this.state.message} onChange={this.handleChange} placeholder={'Write a message...'}
                           autoFocus={true} type="text"/>
                </div>
                <div className={`${styles.btn} flex-centered`}>
                    <button onClick={this.handleSubmit} className={'flex-centered'}>
                        Send<i className="material-icons">send</i>
                    </button>
                </div>
            </div>
        );
    }
}

MessageInput.propTypes = {
    sendMessage: PropTypes.func.isRequired,
}

export default MessageInput;

