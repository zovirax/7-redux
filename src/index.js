import React from 'react';
import ReactDOM from 'react-dom';
import Chat from './containers/Chat/index';
import EditMessage from './components/EditMessage/index';
import store from './store';
import {Provider} from 'react-redux';

import './styles/reset.css';
import './styles/common.css';

const root = document.getElementById('root');
ReactDOM.render(
    <Provider store={store}>
        <React.StrictMode>
            <Chat/>
            <EditMessage/>
        </React.StrictMode>
    </Provider>,
    root);
