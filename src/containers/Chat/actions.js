import {ADD_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE, ITEMS_FETCH_DATA_SUCCESS} from './actionTypes';
import {v4} from 'uuid';

export const addMessage = data => ({
    type: ADD_MESSAGE,
    payload: {
        id: v4(),
        ...data
    }
});

export const editMessage = (id, data) => ({
    type: EDIT_MESSAGE,
    payload: {
        id,
        data
    }
});

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export function itemsFetchDataSuccess(items) {
    return {
        type: ITEMS_FETCH_DATA_SUCCESS,
        items
    };
}

export function itemsFetchData() {
    const url = 'https://edikdolynskyi.github.io/react_sources/messages.json';
    return (dispatch) => {
        (fetch(url)
            .then((response) => response.json())
            .then((items) => dispatch(itemsFetchDataSuccess(items))))
    };
}
