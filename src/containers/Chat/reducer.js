import {ADD_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE, ITEMS_FETCH_DATA_SUCCESS} from './actionTypes';

export default (state = [], action) => {
    switch (action.type) {
        case ADD_MESSAGE: {
            return [...state, action.payload];
        }
        case EDIT_MESSAGE: {
            const {id, data} = action.payload;
            return state.map(message => {
                if (message.id === id && message.text !== data.text) {
                    return {id, ...data};
                }
                return message;
            })
        }
        case DELETE_MESSAGE: {
            const {id} = action.payload;
            return state.filter(message => message.id !== id);
        }
        case ITEMS_FETCH_DATA_SUCCESS:
            return action.items;
        default:
            return state;
    }
}
